﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public class Node
    {
        public Dictionary<char, Node> Childs { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }
        public int CountNode { get; set; } = 0;
        public void Add(string value, char c)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Childs.ContainsKey(next))
            {
                Node node = new Node();
                Childs.Add(next, node);
            }
            Childs[next].CountNode++;
            Childs[next].Add(remaining, c);
        }
        public bool Find(string value)
        {
            if (value.Length == 0)
            {
                return Count > 0;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);
            if (!Childs.ContainsKey(next))
            {
                return false;
            }
            return Childs[next].Find(remaining);
        }
        public Dictionary<string, int> Calculate(string start)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            if (Count > 0)
            {
                result.Add(start, Count);
            }
            foreach (var keyValuePair in Childs)
            {
                Dictionary<string, int> tmp = keyValuePair.Value.Calculate(start + keyValuePair.Key);
                foreach (var kvp in tmp)
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }
            return result;
        }
        public void PrintToTreeNode(TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePair in Childs)
            {
                treeNode.Nodes.Add(keyValuePair.Key.ToString());
                keyValuePair.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }
        public bool Delete(string word)
        {
            if (word == "")
            {
                if (Childs.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            else
            {
                char next = word[0];
                string remaining = word.Length == 1 ? "" : word.Substring(1);

                if (Childs.ContainsKey(next))
                {
                    if (Childs[next].Delete(remaining))
                    {
                        Childs.Remove(next);
                        if (Childs.Count > 0)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }
        public int CountWord(char c, ref int result)
        {
            foreach (var keyValuePair in Childs)
            { 
                if (keyValuePair.Key == c)
                {
                    result++;
                    keyValuePair.Value.CountNode--;
                    if (keyValuePair.Value.CountNode == 0)
                    {
                        return result;
                    }
                }
                keyValuePair.Value.CountWord(c, ref result);
            }
            return result;
        }
    }
}
