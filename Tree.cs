﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    class Tree
    {
        public Node Root { get; set; } = new Node();
        public void Add(string value, char c)
        {
            Root.Add(value.ToLower(), c);
        }
        public bool Find (string value)
        {
           return Root.Find(value.ToLower());
        }
        public Dictionary<string,int> Calculate()
        {
            return Root.Calculate("");
        }
        public void PrintToTreeView (TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintToTreeNode(treeView.Nodes[0]);
            }
        }
        public void Delete (string word)
        {
            if (Root != null)
            {
                Root.Delete(word.ToLower());
            }
        }
        public int CountWord (char c)
        {
            int result = 0;
            int count = Root.CountWord(c, ref result);
            return count;
        }
    }
}
