﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class Form1 : Form
    {
        Tree Tree = new Tree();
        public Form1()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word, 'а');
                    }
                }
            }
            Redraw();
        }

        private void countWords_Click(object sender, EventArgs e)
        {
            Dictionary<string, int> result = Tree.Calculate();
            CountForm countForm = new CountForm(result);
            countForm.ShowDialog();
        }
        private void Redraw()
        {
            treeView1.Nodes.Clear();
            Tree.PrintToTreeView(treeView1);
            treeView1.ExpandAll();
        }

        private void delete_Click(object sender, EventArgs e)
        {
            Tree.Delete(textBox1.Text);
            Redraw();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = Tree.CountWord('а');
            MessageBox.Show(Convert.ToString(count));
        }
    }
}
